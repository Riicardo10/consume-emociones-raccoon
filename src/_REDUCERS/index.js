import {combineReducers} from 'redux';
import emocionesReducer from './emocionesReducer';

export default combineReducers({
    emociones: emocionesReducer
});

import { LISTAR_EMOCIONES } from '../_ACTIONS/types';

const initialState = {
    emociones: [],
}

export default function(state = initialState, action) {
    switch(action.type) {
        case LISTAR_EMOCIONES:
            return {
                ...state,
                emociones: action.payload
            }
        default:
            return state;
    }
}
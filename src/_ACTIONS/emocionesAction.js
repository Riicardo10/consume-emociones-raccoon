import { LISTAR_EMOCIONES } from './types';
import axios from 'axios';

export const listarEmociones = () => async dispatch => {
    try {
        const respuesta = await axios.get( 'http://54.196.43.91:3000/raccoon/feelings' );
        dispatch( {
            type: LISTAR_EMOCIONES,
            payload: respuesta.data
        } )
    } catch (error) {
        dispatch({
            type: LISTAR_EMOCIONES,
            payload: { data: { ok: false, error: 'No se pudo conectar al servidor', data: {} } }
        })
    }
}
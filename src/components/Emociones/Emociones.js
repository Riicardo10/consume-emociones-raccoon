import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Table, Button, Row, Col, Modal } from 'antd';
import { listarEmociones }      from '../../_ACTIONS/emocionesAction';
import { COLUMNAS_EMOCIONES }   from './EmocionesColumnsTable'

class Emociones extends Component {

    state = {
        dataSource: [],
        lengthDataSource: 0,
        cargandoDataSource: true
    }
    
    componentWillMount() {
        this.props.listarEmociones();
    }
    componentWillReceiveProps(nextProps){
        if ( nextProps.emociones !== this.props.emociones ) 
            this.actualizarTabla( nextProps.emociones.emociones.feelings );
    }

    actualizarTabla = data => {
        console.log(data)
        if( data ) {
            data.forEach( element => {
                element['key']     = element.id;
                element['imgUrl']  = <img alt={element.desc} src={element.imgUrl} width='150'></img>;
                element['elegir']  = <Button type="primary" onClick={ () => this.eleccionModal( element.desc )}> Elegir </Button>;
                element['detalle'] = <Button type="primary" onClick={ () => this.informacionModal( element.id, element.desc, element.imgUrl ) }> Detalle </Button>;
            })
            this.setState( { dataSource: data, lengthDataSource: data.length, cargandoDataSource: false } );
        }
    }
    informacionModal = (id, descripcion, img) => {
        Modal.info( {
          title: 'Detalle.',
          content: (    <div>
                            <h3> Descripción de la emoción: {descripcion} </h3>
                            <center> <img alt={descripcion} src={img.props.src} width='200'></img> </center>
                        </div> )
        } );
    }
    eleccionModal = descripcion => {
        Modal.success( {
            title: 'Elección.',
            content: <h3> Elegiste la emoción: {descripcion} </h3>
        } );
    }

    render() {
        return (
            <div style={ { background:'white' } } >
                <Row>
                    <Col xs={3}  sm={3}  md={3} ></Col>
                    <Col xs={18} sm={18} md={18} style={ { paddingTop:'2%', paddingLeft: '4%' } }>
                        <h2> Listado de emociones ( { this.state.lengthDataSource } ) </h2>
                    </Col>
                </Row>
                <Row >
                    <Col xs={2}  sm={5}  md={4}  ></Col>
                    <Col xs={20} sm={12} md={16} >
                        <Table 
                            loading     = { this.state.cargandoDataSource }
                            columns     = { COLUMNAS_EMOCIONES } 
                            dataSource  = { this.state.dataSource } />
                            { this.state.cargandoDataSource ? <h3> Si la prueba es remota, brinde los permisos necesarios del servidor para acceder al recurso </h3> : '' }
                            { this.state.cargandoDataSource ? <h3> Si la prueba es local, en Google Chrome instalar y activar la extensión: Allow-Control-Allow-Origin: * </h3> : '' }
                    </Col>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    emociones:  state.emociones
})
export default connect(mapStateToProps, { 
    listarEmociones
})(Emociones);
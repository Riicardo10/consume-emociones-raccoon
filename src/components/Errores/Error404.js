import React, { Component } from 'react';
import { Row, Col } from 'antd';

class Error404 extends Component {
    render() {
        return (
            <div>
                <Row>
                    <Col xs={8}  sm={8}  md={8}  ></Col>
                    <Col xs={8}  sm={8}  md={8} >
                        <center> <h1 style={{marginTop: '20%'}}> OOPS! </h1> </center>
                    </Col>
                </Row>
                <Row>
                    <Col xs={0}  sm={5}  md={8}  ></Col>
                    <Col xs={6}  sm={8}  md={8} >
                        <center> <img alt='Error' src='https://media.istockphoto.com/vectors/error-404-page-not-found-vector-id673253106' width='400'></img> </center>
                    </Col>
                </Row>
                <Row>
                    <Col xs={6}  sm={8}  md={8}  ></Col>
                    <Col xs={12}  sm={8}  md={8} >
                        <center> <a href='/emociones'> Ver lista de emociones </a> </center>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Error404;
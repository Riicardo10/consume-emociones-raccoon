import React, { Component } from 'react';
import {Provider} from 'react-redux'
import store from './_store/store'

import Emociones      from './components/Emociones/Emociones';
import Error404       from './components/Errores/Error404';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <Provider store={ store }>
        <Router>
          <React.Fragment>
              <Switch>
                <Route exact path='/'           component={ Emociones } />
                <Route exact path='/emociones'  component={ Emociones } />
                <Route component={ Error404 } />
              </Switch>
          </React.Fragment>
        </Router>
      </Provider>
    );
  }
}

export default App;

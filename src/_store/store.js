import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import mainReducer from '../_REDUCERS'

const initialState = {}
const middleware = [thunk]
const store = createStore(
    mainReducer, initialState,
    compose(
        applyMiddleware(...middleware),
        window.navigator.userAgent.includes('Chrome') ?
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() : compose,
    ),
)

// var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
// alert(isSafari)

export default store